package com.example.appmaqueta

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var seguidores :Int=150
        btnCerrar.setOnClickListener {
            finish()
        }
        btnSeguir.setOnClickListener {
            seguidores++
            tvFollowers.text = "$seguidores Followers"
        }
    }
}